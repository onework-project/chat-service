package grpc_client

import (
	"fmt"

	"gitlab.com/onework-project/chat-service/config"
	pba "gitlab.com/onework-project/chat-service/genproto/applicant_service"
	pb "gitlab.com/onework-project/chat-service/genproto/auth_service"
	pbc "gitlab.com/onework-project/chat-service/genproto/company_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	ApplicantService() pba.ApplicantServiceClient
	CompanyService() pbc.CompanyServiceClient
	UserService() pb.UserServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connApplicantService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort)
	}
	connCompanyService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.AuthServiceHost, cfg.AuthServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %v port: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort)
	}
	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"applicant_service": pba.NewApplicantServiceClient(connApplicantService),
			"company_service":   pbc.NewCompanyServiceClient(connCompanyService),
			"user_service":      pb.NewUserServiceClient(connUserService),
		},
	}, nil
}

func (g *GrpcClient) ApplicantService() pba.ApplicantServiceClient {
	return g.connections["applicant_service"].(pba.ApplicantServiceClient)
}

func (g *GrpcClient) CompanyService() pbc.CompanyServiceClient {
	return g.connections["company_service"].(pbc.CompanyServiceClient)
}

func (g *GrpcClient) UserService() pb.UserServiceClient {
	return g.connections["user_service"].(pb.UserServiceClient)
}
