package main

import (
	"fmt"
	"log"
	"net"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/onework-project/chat-service/config"
	pb "gitlab.com/onework-project/chat-service/genproto/chat_service"
	grpcPkg "gitlab.com/onework-project/chat-service/pkg/grpc_client"
	"gitlab.com/onework-project/chat-service/pkg/logger"
	"gitlab.com/onework-project/chat-service/service"
	"gitlab.com/onework-project/chat-service/storage"
)

func main() {
	cfg := config.Load(".")

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	strg := storage.NewStoragePg(psqlConn)

	grpcConn, err := grpcPkg.New(cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connections: %v", err)
	}
	logger.Init()
	logrus := logger.GetLogger()

	chatService := service.NewChatService(strg, &logrus, grpcConn)
	messageService := service.NewMessageService(strg, &logrus)

	lis, err := net.Listen("tcp", cfg.GrpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	reflection.Register(s)

	pb.RegisterChatServiceServer(s, chatService)
	pb.RegisterChatMessageServiceServer(s, messageService)

	log.Println("Grpc server started in port ", cfg.GrpcPort)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
