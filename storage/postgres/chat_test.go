package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/chat-service/storage/repo"
)

func createChat(t *testing.T, id1, id2 int64) *repo.Chat {
	chat, err := strg.Chat().Create(&repo.Chat{
		MemberID:    id1,
		ApplicantID: id2,
	})
	require.NoError(t, err)
	require.NotZero(t, chat.ID)
	require.NotZero(t, chat.CreatedAt)
	require.Equal(t, chat.MemberID, id1)
	require.Equal(t, chat.ApplicantID, id2)

	return chat
}

func deleteChat(t *testing.T, chatId, userId int64) {
	err := strg.Chat().Delete(chatId, userId)
	require.NoError(t, err)
}

func TestCreateChat(t *testing.T) {
	chat := createChat(t, 1, 2)
	deleteChat(t, chat.ID, chat.MemberID)
}

func TestGetAllChats(t *testing.T) {
	id := []int64{}
	userId := []int64{}
	for i := 1; i <= 10; i++ {
		chat := createChat(t, int64(i), int64(i+1))
		id = append(id, chat.ID)
		userId = append(userId, chat.MemberID)
	}
	chats, err := strg.Chat().GetAll(&repo.GetAllChatsParams{
		Limit: 10,
		Page:  1,
	})
	require.NoError(t, err)
	require.Equal(t, len(chats.Chats), 10)

	for i := 0; i < 10; i++ {
		deleteChat(t, id[i], userId[i])
	}
}
