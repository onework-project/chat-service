package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/chat-service/storage/repo"
)

func createChatMessage(t *testing.T) *repo.ChatMessage {
	chat := createChat(t, 1, 2)
	message, err := strg.ChatMessage().Create(&repo.ChatMessage{
		UserID:  1,
		ChatID:  chat.ID,
		Message: "Hello",
	})
	require.NoError(t, err)
	require.NotZero(t, message.ID)
	require.NotZero(t, message.CreatedAt)
	require.Equal(t, message.UserID, int64(1))
	require.Equal(t, message.ChatID, chat.ID)

	return message
}

func deleteChatMessage(t *testing.T, id, userId int64) {
	err := strg.ChatMessage().Delete(id, userId)
	require.NoError(t, err)
}

func TestCreateChatMessage(t *testing.T) {
	message := createChatMessage(t)
	deleteChatMessage(t, message.ID, message.UserID)
}

func TestGetAllChatsMessages(t *testing.T) {
	id := []int64{}
	userId := []int64{}
	for i := 1; i <= 10; i++ {
		message := createChatMessage(t)
		id = append(id, message.ID)
		userId = append(userId, message.UserID)
	}
	messages, err := strg.ChatMessage().GetAll(&repo.GetAllChatsMessagesParams{
		Limit: 10,
		Page:  1,
	})
	require.NoError(t, err)
	require.Equal(t, len(messages.Messages), 10)

	for i := 0; i < 10; i++ {
		deleteChatMessage(t, id[i], userId[i])
	}
}
