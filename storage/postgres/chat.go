package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/chat-service/storage/repo"
)

type chatRepo struct {
	db *sqlx.DB
}

func NewChat(db *sqlx.DB) repo.ChatStrogeI {
	return &chatRepo{
		db: db,
	}
}

func (c *chatRepo) Create(chat *repo.Chat) (*repo.Chat, error) {
	query := `
		INSERT INTO chats (
			member_id,
			applicant_id
		) VALUES ($1, $2)
		RETURNING id, created_at
	`

	err := c.db.QueryRow(
		query,
		chat.MemberID,
		chat.ApplicantID,
	).Scan(
		&chat.ID,
		&chat.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return chat, nil
}

func (c *chatRepo) Get(chatId int64) (*repo.Chat, error) {
	query := `
		SELECT 
			id,
			member_id,
			applicant_id,
			created_at
		FROM chats WHERE id = $1
	`
	var chat repo.Chat
	err := c.db.QueryRow(
		query,
		chatId,
	).Scan(
		&chat.ID,
		&chat.MemberID,
		&chat.ApplicantID,
		&chat.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &chat, nil
}

func (c *chatRepo) Delete(chatId, userId int64) error {
	query := `
		DELETE FROM chats WHERE id = $1 AND member_id = $2 OR applicant_id = $3  
	`
	res, err := c.db.Exec(query, chatId, userId, userId)
	if err != nil {
		return nil
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (c *chatRepo) AutoDelete(userId int64) error {
	query := `
		DELETE FROM chats WHERE member_id = $1 OR applicant_id = $2  
	`
	res, err := c.db.Exec(query, userId, userId)
	if err != nil {
		return nil
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (c *chatRepo) GetAll(params *repo.GetAllChatsParams) (*repo.GetAllChats, error) {
	res := repo.GetAllChats{
		Chats: make([]*repo.Chat, 0),
	}
	offset := (params.Page - 1) * params.Limit
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := ""
	if params.UserID > 0 {
		filter = fmt.Sprintf(" WHERE member_id = %d OR applicant_id = %d ", params.UserID, params.UserID)
	}

	query := `
		SELECT 
			id, 
			member_id,
			applicant_id,
			created_at
		FROM chats
	` + filter + `
		ORDER BY created_at DESC
	` + limit

	rows, err := c.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var chat repo.Chat
		err := rows.Scan(
			&chat.ID,
			&chat.MemberID,
			&chat.ApplicantID,
			&chat.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		res.Chats = append(res.Chats, &chat)
	}

	queryCount := ` SELECT count(1) FROM chats ` + filter
	err = c.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
