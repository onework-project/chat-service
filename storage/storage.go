package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/chat-service/storage/postgres"
	"gitlab.com/onework-project/chat-service/storage/repo"
)

type StorageI interface {
	Chat() repo.ChatStrogeI
	ChatMessage() repo.ChatMessageStrogeI
}

type storagePg struct {
	chatRepo        repo.ChatStrogeI
	chatMessageRepo repo.ChatMessageStrogeI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		chatRepo:        postgres.NewChat(db),
		chatMessageRepo: postgres.NewChatMessage(db),
	}
}

func (s *storagePg) Chat() repo.ChatStrogeI {
	return s.chatRepo
}

func (s *storagePg) ChatMessage() repo.ChatMessageStrogeI {
	return s.chatMessageRepo
}
