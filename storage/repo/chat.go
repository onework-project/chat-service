package repo

import "time"

type ChatStrogeI interface {
	Create(c *Chat) (*Chat, error)
	Get(chat_id int64) (*Chat, error)
	Delete(chatId, userId int64) error
	AutoDelete(userId int64) error
	GetAll(params *GetAllChatsParams) (*GetAllChats, error)
}

type Chat struct {
	ID          int64
	MemberID    int64
	ApplicantID int64
	CreatedAt   time.Time
}

type GetAllChatsParams struct {
	Limit  int64
	Page   int64
	UserID int64
}

type GetAllChats struct {
	Chats []*Chat
	Count int64
}
