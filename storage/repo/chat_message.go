package repo

import "time"

type ChatMessageStrogeI interface {
	Create(m *ChatMessage) (*ChatMessage, error)
	Update(m *ChatMessage) (*ChatMessage, error)
	Delete(id, userId int64) error
	GetAll(params *GetAllChatsMessagesParams) (*GetAllChatsMessages, error)
}

type ChatMessage struct {
	ID        int64
	UserID    int64
	ChatID    int64
	Message   string
	CreatedAt time.Time
}

type GetAllChatsMessages struct {
	Messages []*ChatMessage
	Count    int64
}

type GetAllChatsMessagesParams struct {
	Limit  int64
	Page   int64
	ChatID int64
	Search string
}
