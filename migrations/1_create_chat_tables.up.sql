CREATE TABLE IF NOT EXISTS "chats" (
    "id" SERIAL PRIMARY KEY,
    "member_id" int NOT NULL CHECK ("member_id" > 0),
    "applicant_id" int NOT NULL  CHECK ("applicant_id" > 0),
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "chat_messages" (
    "id" SERIAL PRIMARY KEY,
    "message" TEXT NOT NULL,
    "user_id" INT NOT NULL CHECK ("user_id" > 0),
    "chat_id" INT NOT NULL REFERENCES chats(id) ON DELETE CASCADE,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);