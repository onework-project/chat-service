package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	pb "gitlab.com/onework-project/chat-service/genproto/chat_service"
	"gitlab.com/onework-project/chat-service/pkg/logger"
	"gitlab.com/onework-project/chat-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/onework-project/chat-service/storage"
)

type MessageService struct {
	pb.UnimplementedChatMessageServiceServer
	storage storage.StorageI
	logger  *logger.Logger
}

func NewMessageService(strg storage.StorageI, logger *logger.Logger) *MessageService {
	return &MessageService{
		storage: strg,
		logger:  logger,
	}
}

func (s *MessageService) Create(ctx context.Context, req *pb.CreateChatMessageReq) (*pb.ChatMessage, error) {
	s.logger.Info("create message")
	chat, err := s.storage.ChatMessage().Create(&repo.ChatMessage{
		UserID:  req.UserId,
		ChatID:  req.ChatId,
		Message: req.Message,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create message")
		return nil, status.Errorf(codes.Internal, "failed to create: %v", err)
	}

	return parseMessageModel(chat), nil
}

func (s *MessageService) Update(ctx context.Context, req *pb.ChatMessage) (*pb.ChatMessage, error) {
	chat, err := s.storage.ChatMessage().Update(&repo.ChatMessage{
		ID:      req.Id,
		Message: req.Message,
		UserID:  req.UserId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update message")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to update: %v", err)
	}

	return parseMessageModel(chat), nil
}

func (s *MessageService) Delete(ctx context.Context, req *pb.DeleteChatMessageReq) (*emptypb.Empty, error) {
	err := s.storage.ChatMessage().Delete(req.Id, req.UserId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete message")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *MessageService) GetAll(ctx context.Context, req *pb.GetAllChatsMessagesParams) (*pb.GetAllChatsMessagesRes, error) {
	messages, err := s.storage.ChatMessage().GetAll(&repo.GetAllChatsMessagesParams{
		Limit:  req.Limit,
		Page:   req.Page,
		ChatID: req.ChatId,
		Search: req.Search,
	})

	if err != nil {
		s.logger.WithError(err).Error("failed to get all messages")
		return nil, status.Errorf(codes.Internal, "failed to get all: %w", err)
	}

	response := pb.GetAllChatsMessagesRes{
		ChatMessages: make([]*pb.ChatMessage, 0),
		Count:        messages.Count,
	}
	for _, v := range messages.Messages {
		res := parseMessageModel(v)
		response.ChatMessages = append(response.ChatMessages, res)
	}

	return &response, nil
}

func parseMessageModel(res *repo.ChatMessage) *pb.ChatMessage {
	return &pb.ChatMessage{
		Id:        res.ID,
		Message:   res.Message,
		UserId:    res.UserID,
		ChatId:    res.ChatID,
		CreatedAt: res.CreatedAt.Format(time.RFC3339),
	}
}
