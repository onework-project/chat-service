package service

import (
	"context"
	"database/sql"
	"errors"
	"sync"
	"time"

	"gitlab.com/onework-project/chat-service/genproto/applicant_service"
	"gitlab.com/onework-project/chat-service/genproto/auth_service"
	pb "gitlab.com/onework-project/chat-service/genproto/chat_service"
	"gitlab.com/onework-project/chat-service/genproto/company_service"
	"gitlab.com/onework-project/chat-service/pkg/grpc_client"
	"gitlab.com/onework-project/chat-service/pkg/logger"
	"gitlab.com/onework-project/chat-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/onework-project/chat-service/storage"
)

type ChatService struct {
	pb.UnimplementedChatServiceServer
	storage storage.StorageI
	logger  *logger.Logger
	grpcCon grpc_client.GrpcClientI
}

func NewChatService(strg storage.StorageI, logger *logger.Logger, grpcCon grpc_client.GrpcClientI) *ChatService {
	return &ChatService{
		storage: strg,
		logger:  logger,
		grpcCon: grpcCon,
	}
}

func (s *ChatService) Create(ctx context.Context, req *pb.CreateOrDeleteChatReq) (*pb.Chat, error) {
	s.logger.Info("create chat")
	chat, err := s.storage.Chat().Create(&repo.Chat{
		MemberID:    req.FId,
		ApplicantID: req.SId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create chat")
		return nil, status.Errorf(codes.Internal, "failed to create: %v", err)
	}
	member, err := s.grpcCon.UserService().Get(context.Background(), &auth_service.GetByIdRequest{Id: req.FId})
	if err != nil {
		s.logger.WithError(err).Error("failed to get member")
		return nil, status.Errorf(codes.Internal, "failed to create: %v", err)
	}

	return s.parseChatModel(chat, member), nil
}

func (s *ChatService) Get(ctx context.Context, req *pb.GetChatIdReq) (*pb.Chat, error) {
	s.logger.Info("get chat")
	chat, err := s.storage.Chat().Get(req.ChatId)
	if err != nil {
		s.logger.WithError(err).Error("failed to get chat")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "failed to create: %v", err)
	}
	member, err := s.grpcCon.UserService().Get(context.Background(), &auth_service.GetByIdRequest{Id: chat.MemberID})
	if err != nil {
		s.logger.WithError(err).Error("failed to get member")
		return nil, status.Errorf(codes.Internal, "failed to create: %v", err)
	}

	return s.parseChatModel(chat, member), nil
}

func (s *ChatService) GetTwoInfo(chat *pb.Chat, member *auth_service.User) {
	var wg sync.WaitGroup
	wg.Add(2)
	var err error
	var companyInfo *company_service.Company
	var applicantInfo *applicant_service.Applicant
	go func() {
		defer wg.Done()
		companyInfo, err = s.grpcCon.CompanyService().Get(context.Background(), &company_service.CompanyIDReq{Id: member.CompanyId})
		if err != nil {
			s.logger.WithError(err).Error("failed to get company info")
		} else {
			chat.CompanyInfo = &pb.ChatCompanyInfo{
				Id:   companyInfo.Id,
				Name: companyInfo.CompanyName,
			}
		}
	}()

	go func() {
		defer wg.Done()
		applicantInfo, err = s.grpcCon.ApplicantService().Get(context.Background(), &applicant_service.IdRequest{Id: chat.ApplicantId})
		if err != nil {
			s.logger.WithError(err).Error("failed to get applicant info")
		} else {
			chat.ApplicantInfo = &pb.ChatApplicantInfo{
				Id:       applicantInfo.Id,
				Fullname: applicantInfo.FirstName + " " + applicantInfo.LastName,
			}
		}
	}()
	wg.Wait()
}

func (s *ChatService) parseChatModel(res *repo.Chat, member *auth_service.User) *pb.Chat {
	chat := pb.Chat{
		Id:          res.ID,
		MemberId:    res.MemberID,
		ApplicantId: res.ApplicantID,
		CreatedAt:   res.CreatedAt.Format(time.RFC3339),
	}
	s.GetTwoInfo(&chat, member)
	return &chat
}

func (s *ChatService) Delete(ctx context.Context, req *pb.CreateOrDeleteChatReq) (*emptypb.Empty, error) {
	err := s.storage.Chat().Delete(req.FId, req.SId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete chat")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *ChatService) AutoDelete(ctx context.Context, req *pb.AutoDeleteIdReq) (*emptypb.Empty, error) {
	err := s.storage.Chat().AutoDelete(req.UserId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete chat")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to delete: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *ChatService) GetAll(ctx context.Context, req *pb.GetAllChatsParams) (*pb.GetAllChatsRes, error) {
	privateChats, err := s.storage.Chat().GetAll(&repo.GetAllChatsParams{
		Limit:  req.Limit,
		Page:   req.Page,
		UserID: req.UserId,
	})

	if err != nil {
		s.logger.WithError(err).Error("failed to get all private chats")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "failed to get all: %w", err)
	}

	response := pb.GetAllChatsRes{
		Chats: make([]*pb.Chat, 0),
		Count: privateChats.Count,
	}
	for _, v := range privateChats.Chats {
		member, err := s.grpcCon.UserService().Get(context.Background(), &auth_service.GetByIdRequest{Id: v.MemberID})
		if err != nil {
			s.logger.WithError(err).Error("failed to get member")
			return nil, status.Errorf(codes.Internal, "failed to create: %v", err)
		}
		res := s.parseChatModel(v, member)
		response.Chats = append(response.Chats, res)
	}

	return &response, nil
}
