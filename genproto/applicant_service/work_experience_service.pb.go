// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.12
// source: work_experience_service.proto

package applicant_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type WorkExp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id             int64  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	ApplicantId    int64  `protobuf:"varint,2,opt,name=applicant_id,json=applicantId,proto3" json:"applicant_id,omitempty"`
	CompanyName    string `protobuf:"bytes,3,opt,name=company_name,json=companyName,proto3" json:"company_name,omitempty"`
	WorkPosition   string `protobuf:"bytes,4,opt,name=work_position,json=workPosition,proto3" json:"work_position,omitempty"`
	CompanyWebsite string `protobuf:"bytes,5,opt,name=company_website,json=companyWebsite,proto3" json:"company_website,omitempty"`
	EmploymentType string `protobuf:"bytes,6,opt,name=employment_type,json=employmentType,proto3" json:"employment_type,omitempty"`
	StartYear      string `protobuf:"bytes,7,opt,name=start_year,json=startYear,proto3" json:"start_year,omitempty"`
	StartMonth     string `protobuf:"bytes,8,opt,name=start_month,json=startMonth,proto3" json:"start_month,omitempty"`
	EndYear        string `protobuf:"bytes,9,opt,name=end_year,json=endYear,proto3" json:"end_year,omitempty"`
	EndMonth       string `protobuf:"bytes,10,opt,name=end_month,json=endMonth,proto3" json:"end_month,omitempty"`
	Description    string `protobuf:"bytes,11,opt,name=description,proto3" json:"description,omitempty"`
}

func (x *WorkExp) Reset() {
	*x = WorkExp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_work_experience_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *WorkExp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*WorkExp) ProtoMessage() {}

func (x *WorkExp) ProtoReflect() protoreflect.Message {
	mi := &file_work_experience_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use WorkExp.ProtoReflect.Descriptor instead.
func (*WorkExp) Descriptor() ([]byte, []int) {
	return file_work_experience_service_proto_rawDescGZIP(), []int{0}
}

func (x *WorkExp) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *WorkExp) GetApplicantId() int64 {
	if x != nil {
		return x.ApplicantId
	}
	return 0
}

func (x *WorkExp) GetCompanyName() string {
	if x != nil {
		return x.CompanyName
	}
	return ""
}

func (x *WorkExp) GetWorkPosition() string {
	if x != nil {
		return x.WorkPosition
	}
	return ""
}

func (x *WorkExp) GetCompanyWebsite() string {
	if x != nil {
		return x.CompanyWebsite
	}
	return ""
}

func (x *WorkExp) GetEmploymentType() string {
	if x != nil {
		return x.EmploymentType
	}
	return ""
}

func (x *WorkExp) GetStartYear() string {
	if x != nil {
		return x.StartYear
	}
	return ""
}

func (x *WorkExp) GetStartMonth() string {
	if x != nil {
		return x.StartMonth
	}
	return ""
}

func (x *WorkExp) GetEndYear() string {
	if x != nil {
		return x.EndYear
	}
	return ""
}

func (x *WorkExp) GetEndMonth() string {
	if x != nil {
		return x.EndMonth
	}
	return ""
}

func (x *WorkExp) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

type GetAllWorkExps struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	WorkExps []*WorkExp `protobuf:"bytes,1,rep,name=work_exps,json=workExps,proto3" json:"work_exps,omitempty"`
	Count    int64      `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
}

func (x *GetAllWorkExps) Reset() {
	*x = GetAllWorkExps{}
	if protoimpl.UnsafeEnabled {
		mi := &file_work_experience_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllWorkExps) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllWorkExps) ProtoMessage() {}

func (x *GetAllWorkExps) ProtoReflect() protoreflect.Message {
	mi := &file_work_experience_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllWorkExps.ProtoReflect.Descriptor instead.
func (*GetAllWorkExps) Descriptor() ([]byte, []int) {
	return file_work_experience_service_proto_rawDescGZIP(), []int{1}
}

func (x *GetAllWorkExps) GetWorkExps() []*WorkExp {
	if x != nil {
		return x.WorkExps
	}
	return nil
}

func (x *GetAllWorkExps) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

var File_work_experience_service_proto protoreflect.FileDescriptor

var file_work_experience_service_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x77, 0x6f, 0x72, 0x6b, 0x5f, 0x65, 0x78, 0x70, 0x65, 0x72, 0x69, 0x65, 0x6e, 0x63,
	0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x08, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0d, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xf0, 0x02, 0x0a, 0x07, 0x57, 0x6f, 0x72,
	0x6b, 0x45, 0x78, 0x70, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x02, 0x69, 0x64, 0x12, 0x21, 0x0a, 0x0c, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x6e,
	0x74, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x61, 0x70, 0x70, 0x6c,
	0x69, 0x63, 0x61, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x21, 0x0a, 0x0c, 0x63, 0x6f, 0x6d, 0x70, 0x61,
	0x6e, 0x79, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x63,
	0x6f, 0x6d, 0x70, 0x61, 0x6e, 0x79, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x23, 0x0a, 0x0d, 0x77, 0x6f,
	0x72, 0x6b, 0x5f, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0c, 0x77, 0x6f, 0x72, 0x6b, 0x50, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x27, 0x0a, 0x0f, 0x63, 0x6f, 0x6d, 0x70, 0x61, 0x6e, 0x79, 0x5f, 0x77, 0x65, 0x62, 0x73, 0x69,
	0x74, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x63, 0x6f, 0x6d, 0x70, 0x61, 0x6e,
	0x79, 0x57, 0x65, 0x62, 0x73, 0x69, 0x74, 0x65, 0x12, 0x27, 0x0a, 0x0f, 0x65, 0x6d, 0x70, 0x6c,
	0x6f, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0e, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x54, 0x79, 0x70,
	0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x79, 0x65, 0x61, 0x72, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x73, 0x74, 0x61, 0x72, 0x74, 0x59, 0x65, 0x61, 0x72,
	0x12, 0x1f, 0x0a, 0x0b, 0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x6d, 0x6f, 0x6e, 0x74, 0x68, 0x18,
	0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x73, 0x74, 0x61, 0x72, 0x74, 0x4d, 0x6f, 0x6e, 0x74,
	0x68, 0x12, 0x19, 0x0a, 0x08, 0x65, 0x6e, 0x64, 0x5f, 0x79, 0x65, 0x61, 0x72, 0x18, 0x09, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x65, 0x6e, 0x64, 0x59, 0x65, 0x61, 0x72, 0x12, 0x1b, 0x0a, 0x09,
	0x65, 0x6e, 0x64, 0x5f, 0x6d, 0x6f, 0x6e, 0x74, 0x68, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x65, 0x6e, 0x64, 0x4d, 0x6f, 0x6e, 0x74, 0x68, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73,
	0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x56, 0x0a, 0x0e, 0x47,
	0x65, 0x74, 0x41, 0x6c, 0x6c, 0x57, 0x6f, 0x72, 0x6b, 0x45, 0x78, 0x70, 0x73, 0x12, 0x2e, 0x0a,
	0x09, 0x77, 0x6f, 0x72, 0x6b, 0x5f, 0x65, 0x78, 0x70, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x11, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x57, 0x6f, 0x72, 0x6b,
	0x45, 0x78, 0x70, 0x52, 0x08, 0x77, 0x6f, 0x72, 0x6b, 0x45, 0x78, 0x70, 0x73, 0x12, 0x14, 0x0a,
	0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f,
	0x75, 0x6e, 0x74, 0x32, 0x97, 0x02, 0x0a, 0x0e, 0x57, 0x6f, 0x72, 0x6b, 0x45, 0x78, 0x70, 0x53,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x30, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x12, 0x11, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x57, 0x6f, 0x72, 0x6b,
	0x45, 0x78, 0x70, 0x1a, 0x11, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x57,
	0x6f, 0x72, 0x6b, 0x45, 0x78, 0x70, 0x22, 0x00, 0x12, 0x2c, 0x0a, 0x03, 0x47, 0x65, 0x74, 0x12,
	0x10, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x47, 0x65, 0x74, 0x52, 0x65,
	0x71, 0x1a, 0x11, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x57, 0x6f, 0x72,
	0x6b, 0x45, 0x78, 0x70, 0x22, 0x00, 0x12, 0x30, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x12, 0x11, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x57, 0x6f, 0x72, 0x6b,
	0x45, 0x78, 0x70, 0x1a, 0x11, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x57,
	0x6f, 0x72, 0x6b, 0x45, 0x78, 0x70, 0x22, 0x00, 0x12, 0x35, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65,
	0x74, 0x65, 0x12, 0x18, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x44, 0x65,
	0x6c, 0x65, 0x74, 0x65, 0x4f, 0x72, 0x47, 0x65, 0x74, 0x52, 0x65, 0x71, 0x1a, 0x0f, 0x2e, 0x67,
	0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12,
	0x3c, 0x0a, 0x06, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x12, 0x16, 0x2e, 0x67, 0x65, 0x6e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x50, 0x61, 0x72, 0x61, 0x6d,
	0x73, 0x1a, 0x18, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x47, 0x65, 0x74,
	0x41, 0x6c, 0x6c, 0x57, 0x6f, 0x72, 0x6b, 0x45, 0x78, 0x70, 0x73, 0x22, 0x00, 0x42, 0x1c, 0x5a,
	0x1a, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63,
	0x61, 0x6e, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_work_experience_service_proto_rawDescOnce sync.Once
	file_work_experience_service_proto_rawDescData = file_work_experience_service_proto_rawDesc
)

func file_work_experience_service_proto_rawDescGZIP() []byte {
	file_work_experience_service_proto_rawDescOnce.Do(func() {
		file_work_experience_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_work_experience_service_proto_rawDescData)
	})
	return file_work_experience_service_proto_rawDescData
}

var file_work_experience_service_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_work_experience_service_proto_goTypes = []interface{}{
	(*WorkExp)(nil),        // 0: genproto.WorkExp
	(*GetAllWorkExps)(nil), // 1: genproto.GetAllWorkExps
	(*GetReq)(nil),         // 2: genproto.GetReq
	(*DeleteOrGetReq)(nil), // 3: genproto.DeleteOrGetReq
	(*GetAllParams)(nil),   // 4: genproto.GetAllParams
	(*Empty)(nil),          // 5: genproto.Empty
}
var file_work_experience_service_proto_depIdxs = []int32{
	0, // 0: genproto.GetAllWorkExps.work_exps:type_name -> genproto.WorkExp
	0, // 1: genproto.WorkExpService.Create:input_type -> genproto.WorkExp
	2, // 2: genproto.WorkExpService.Get:input_type -> genproto.GetReq
	0, // 3: genproto.WorkExpService.Update:input_type -> genproto.WorkExp
	3, // 4: genproto.WorkExpService.Delete:input_type -> genproto.DeleteOrGetReq
	4, // 5: genproto.WorkExpService.GetAll:input_type -> genproto.GetAllParams
	0, // 6: genproto.WorkExpService.Create:output_type -> genproto.WorkExp
	0, // 7: genproto.WorkExpService.Get:output_type -> genproto.WorkExp
	0, // 8: genproto.WorkExpService.Update:output_type -> genproto.WorkExp
	5, // 9: genproto.WorkExpService.Delete:output_type -> genproto.Empty
	1, // 10: genproto.WorkExpService.GetAll:output_type -> genproto.GetAllWorkExps
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_work_experience_service_proto_init() }
func file_work_experience_service_proto_init() {
	if File_work_experience_service_proto != nil {
		return
	}
	file_request_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_work_experience_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*WorkExp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_work_experience_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllWorkExps); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_work_experience_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_work_experience_service_proto_goTypes,
		DependencyIndexes: file_work_experience_service_proto_depIdxs,
		MessageInfos:      file_work_experience_service_proto_msgTypes,
	}.Build()
	File_work_experience_service_proto = out.File
	file_work_experience_service_proto_rawDesc = nil
	file_work_experience_service_proto_goTypes = nil
	file_work_experience_service_proto_depIdxs = nil
}
